const studenJson = [
    {
        "id": 7, 
        "nombres": "JULIAN LEONARDO",
        "apellidos": "SANCHEZ PRADA",
        "semestre": "QUINTO",
        "paralelo": "A",
        "direccion": "C. Comercial Lorem, 232 11ºE",
        "telefono": 7334084998,
        "correo": "24928572E@gmail.com"
    },
    {
        "id": 4, 
        "nombres": "JULIANA GAVIRIA",
        "apellidos": "GONZALEZ SUAREZ" ,
        "semestre": "QUINTO",
        "paralelo": "A",
        "direccion": "Carrera Lorem, 147B",
        "telefono": 7210958572,
        "correo": "81316387W@gmail.com"
    },
    {
        "id": 1, 
        "nombres": "JULY CATHERINE",
        "apellidos": "HERNÁNDEZ PULIDO" ,
        "semestre": "QUINTO",
        "paralelo": "A",
        "direccion": "Glorieta Lorem ipsum, 34A 12ºC",
        "telefono": 7876948640,
        "correo": "23262369P@gmail.com"
    },
    {
        "id": 14,
        "nombres": "KAREN ELIANA",
        "apellidos": "PUERTO CASTRO",
        "semestre": "QUINTO",
        "paralelo": "B",
        "direccion": "Ronda Lorem ipsum dolor, 12",
        "telefono": 7978289610,
        "correo": "81924724N@gmail.com"
    },
    {
        "id": 10,
        "nombres": "LAURA DIAZ",
        "apellidos": "VARON BUITRAGO",
        "semestre": "QUINTO",
        "paralelo": "A",
        "direccion": "Ronda Lorem ipsum dolor sit, 46A",
        "telefono": 6953068375,
        "correo": "34317614G@gmail.com"
    },
    {
        "id": 2,
        "nombres": "LAURA CAMILA",
        "apellidos": "RODRÍGUEZ TORRES",
        "semestre": "QUINTO",
        "paralelo": "B",
        "direccion": "Calle Lorem ipsum dolor, 35A",
        "telefono": 8172620355,
        "correo": "47146020E@gmail.com"
    },
    {
        "id": 13,
        "nombres": "LAURA CATALINA",
        "apellidos": "RODRÍGUEZ TORRES" ,
        "semestre": "QUINTO",
        "paralelo": "B",
        "direccion": "Callejón Lorem ipsum dolor sit, 257B 15ºH",
        "telefono": 7469464613,
        "correo": "42520142L@gmail.com"
    },
    {
        "id": 6,
        "nombres": "LAURA FERNANDA",
        "apellidos": "NOVOA GOMEZ",
        "semestre": "QUINTO",
        "paralelo": "A",
        "direccion": "Avenida Lorem ipsum, 100B 14ºD",
        "telefono": 691819409,
        "correo": "96135526B@gmail.com"
    },
    {
        "id": 11,
        "nombres": "LAURA FERNANDA",
        "apellidos": "DEL RÍO AYERBE",
        "semestre": "QUINTO",
        "paralelo": "A",
        "direccion": "Acceso Lorem ipsum, 271A 8ºH",
        "telefono": 6540742496,
        "correo": "77831590G@gmail.com"
    },
    {
        "id": 12,
        "nombres": "LAURA NATALIA",
        "apellidos": "DUEÑAS ROJAS",
        "semestre": "QUINTO",
        "paralelo": "B",
        "direccion": "Calle Lorem ipsum, 112A 1ºA",
        "telefono": 8871206858,
        "correo": "43652977N@gmail.com"
    }
]

studenJson.forEach((estudiante)=>{
    const elemento = document.querySelector('.box-estudent');
    const plantilla = document.createElement('div');
    plantilla.innerHTML = `<button class="name-estudiante" data-id="${estudiante.id}">${estudiante.nombres } ${estudiante.apellidos}</button>`;
    elemento.appendChild(plantilla);
})


const estudiantes = document.querySelectorAll('.name-estudiante');

estudiantes.forEach((estudiante)=>{
    estudiante.addEventListener('click', (nombre)=>{
        let id = nombre.target.getAttribute('data-id');
        studenJson.forEach((estudiante)=>{
            if(id == estudiante.id){
                const viewDetalle  = document.querySelector('.group-infor');
                viewDetalle.innerHTML = `
                    <p>Nombres: <span>${estudiante.nombres}</span></p>
                    <p>Apellidos: <span>${estudiante.apellidos}</span></p>
                    <p>Semestre: <span>${estudiante.semestre}</span></p>
                    <p>Paralelo: <span>${estudiante.paralelo}</span></p>
                    <p>Dirección: <span>${estudiante.direccion}</span></p>
                    <p>Teléfono: <span>${estudiante.telefono}</span></p>
                    <p>Correo Electrónico: <span>${estudiante.correo}</span></p>
                `;
            }
        })
    })
})